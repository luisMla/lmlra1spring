package com.luismarinlafuente.servidorSpring;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name ="incidencias_usuarios")
public class IncidenciasUsuarios {

    @Id
    @GeneratedValue
    private int id;
    @Column
    private String titulo;
    @Column
    private String calle;
    @Column
    private String motivo;
    @Column
    private String tramo;
    @Column
    private String observaciones;
    @Column
    private Date inicio;
    @Column
    private Date fin;
    @Column
    private double longitud;
    @Column
    private double latitud;
    @Column
    private String descipcion;
    @Column
    private String tipo;
    @Column(name = "tiene_comentarios")
    private int tieneComentarios;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getTramo() {
        return tramo;
    }

    public void setTramo(String tramo) {
        this.tramo = tramo;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Date getInicio() {
        return inicio;
    }

    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }

    public Date getFin() {
        return fin;
    }

    public void setFin(Date fin) {
        this.fin = fin;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public String getDescipcion() {
        return descipcion;
    }

    public void setDescipcion(String descipcion) {
        this.descipcion = descipcion;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getTieneComentarios() {
        return tieneComentarios;
    }

    public void setTieneComentarios(int tieneComentarios) {
        this.tieneComentarios = tieneComentarios;
    }
}
