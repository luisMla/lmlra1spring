package com.luismarinlafuente.servidorSpring;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IncidenciasUsuariosRepository extends CrudRepository<IncidenciasUsuarios,Integer> {
    IncidenciasUsuarios findAllById(int id);
    List<IncidenciasUsuarios> findAll();
    IncidenciasUsuarios findFirstBy(int id);

    @Override
    IncidenciasUsuarios findOne(Integer id);

    void deleteAll();
}
