package com.luismarinlafuente.servidorSpring;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IncidenciaRepository extends CrudRepository<Incidencia, Integer> {
    List<Incidencia> findAll();
    Incidencia findAllById(int id);
    Incidencia findFirstBy(int id);


    @Override
    Incidencia findOne(Integer id);

    void deleteAll();

    @Override
    void delete(Incidencia entity);

}
