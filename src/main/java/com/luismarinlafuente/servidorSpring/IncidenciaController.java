package com.luismarinlafuente.servidorSpring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class IncidenciaController {

    @Autowired
    private IncidenciaRepository incidenciaRepository;
    @Autowired
    private IncidenciasUsuariosRepository incidenciasUsuariosRepository;
    @Autowired
    private IncidenciaComentadaRepository incidenciaComentadaRepository;
    @Autowired
    private ComentariosRepository comentariosRepository;
    @Autowired
    private UsuarioRepository usuarioRepository;
    @Autowired
    private ReportRepository reportRepository;
    @Autowired
    private ReportAppRepository reportAppRepository;

    @RequestMapping("/report_app")
    public void reportApp(@RequestParam(value = "id_usuario",defaultValue = "0")int idUsuario,
                          @RequestParam(value = "email",defaultValue = "null")String email,
                          @RequestParam(value = "mensaje",defaultValue = "error")String mensaje){
        ReportApp reportApp = new ReportApp();
        reportApp.setIdUsuario(idUsuario);
        reportApp.setEmail(email);
        reportApp.setMensaje(mensaje);
        reportAppRepository.save(reportApp);

    }

    @RequestMapping("/get_usuario")
    public List<Usuario> getUsuario(@RequestParam(value = "loggin",defaultValue ="0")String loggin,
                              @RequestParam(value = "pass",defaultValue = "no aplica")String pass){
        ArrayList<Usuario>usuarioArrayList = new ArrayList<>();
        List<Usuario> usuarios = usuarioRepository.findAll();
        for (Usuario usu : usuarios) {
            if(usu.getLoggin().equals(loggin)&&usu.getPass().equals(pass)){
                usuarioArrayList.add(usu);
            }
        }
        usuarios = usuarioArrayList;
        return usuarios;
    }
    @RequestMapping("/add_usuario")
    public void getUsuario(@RequestParam(value = "loggin",defaultValue ="0")String loggin,
                           @RequestParam(value = "pass",defaultValue = "no aplica")String pass,
                           @RequestParam(value = "email",defaultValue = "no aplica")String email){
        Usuario usuario = new Usuario();
        usuario.setLoggin(loggin);
        usuario.setEmail(email);
        usuario.setPass(pass);
        usuarioRepository.save(usuario);

    }
    @RequestMapping("/report")
    public void report(@RequestParam(value = "id_usuario",defaultValue = "0")int idUsuario,
                       @RequestParam(value = "id_comentario",defaultValue ="0")int idComentario,
                       @RequestParam(value = "motivo",defaultValue = "no aplica")String motivo){
        Report report = new Report();
        report.setIdUsuario(idUsuario);
        report.setIdComentario(idComentario);
        report.setMotivo(motivo);
        reportRepository.save(report);
    }

    @RequestMapping("/incidencias")//todo get incidencias (las del ayuntamiento)
    public List<Incidencia> getIncidencias() {
        List <Incidencia> incidencias = incidenciaRepository.findAll();
        return incidencias;
    }
    @RequestMapping("/incidencias_usuarios")//todo getIncidencias de los usuarios
    public List<IncidenciasUsuarios> getIncidenciasUsuarios() {
        List<IncidenciasUsuarios> incidenciasUsuarios = incidenciasUsuariosRepository.findAll();
        return incidenciasUsuarios;
    }
    @RequestMapping("/incidencias_comentadas")//todo getincidencias Comentadas
    public List<IncidenciaComentada> getIncidenciasComentadas() {
        List<IncidenciaComentada> incidenciaComentadas = incidenciaComentadaRepository.findAll();
        return incidenciaComentadas;
    }
    @RequestMapping("/incidencia_comentada")
    public IncidenciaComentada getIncidenciaComentada(int id){
        IncidenciaComentada incidenciaComentada = null;
        incidenciaComentada = incidenciaComentadaRepository.findOne(id);
        return incidenciaComentada;
    }
    @RequestMapping("/cometar_incidencia")//todo Comentar incidencia
    public void comentarIncidencia(@RequestParam(value = "id_usuario", defaultValue = "0") int idUsuario,
                                   @RequestParam(value = "id_incidencia" , defaultValue = "0") int idIncidencia,
                                   @RequestParam(value = "comentario", defaultValue = "no hay") String comentario){

        boolean nueva = false;
        Comentario comentarios = new Comentario();
        Usuario usuario = null;
        Incidencia incidencia = null;
        IncidenciasUsuarios incidenciasUsuarios = null;
        IncidenciaComentada incidenciaComentada = null;
        usuario = getUsuario(idUsuario);
        incidencia = getPutaIncidencia(idIncidencia);
        incidenciasUsuarios = getIncidenciasU(idIncidencia);
        incidenciaComentada = getIncidenciaComentada(idIncidencia);
        if(usuario != null){
            comentarios.setIdUsuario(usuario.getId());
            comentarios.setLoggin(usuario.getLoggin());
            comentarios.setComentario(comentario);
            if(incidenciaComentada != null){
                comentarios.setIdIncidencia(incidenciaComentada.getId());
                comentariosRepository.save(comentarios);
            }
            else if(incidencia!= null){
                incidenciaComentada = castIncidencia(incidencia,null);
                incidenciaComentadaRepository.save(incidenciaComentada);
                incidenciaRepository.delete(incidencia);
                nueva = true;
            }
            else if(incidenciasUsuarios != null){
                incidenciaComentada = castIncidencia(null,incidenciasUsuarios);
                incidenciaComentadaRepository.save(incidenciaComentada);
                incidenciasUsuariosRepository.delete(incidenciasUsuarios);
                nueva = true;
            }
            if(incidenciaComentada != null && nueva){
                List<IncidenciaComentada> incidenciaComentadas = getIncidenciasComentadas();
                incidenciaComentada = incidenciaComentadas.get(incidenciaComentadas.size()-1);
                comentarios.setIdIncidencia(incidenciaComentada.getId());
                comentariosRepository.save(comentarios);
            }
        }
    }
    private Incidencia getPutaIncidencia(int id){
        ArrayList<Incidencia> incidencias = (ArrayList<Incidencia>) incidenciaRepository.findAll();
        for (Incidencia inci : incidencias) {
            if (inci.getId() == id){
                return inci;
            }
        }
        return null;
    }

    @RequestMapping("/get_comentarios")
    public List<Comentario> getComentariosIncidencia(@RequestParam(value = "id",defaultValue = "0")int idIncidencia){
        List<Comentario>comentarios = comentariosRepository.findAll();
        ArrayList<Comentario> comentarios1= new ArrayList<>();
        for (Comentario comentario : comentarios) {
            if(idIncidencia == comentario.getIdIncidencia()){
                comentarios1.add(comentario);
            }
        }
        comentarios = comentarios1;
        return comentarios;
    }
    @RequestMapping("/comentarios_incidencias")
    public List<Comentario> getComentariosIncidencias(int idIncidencia ){
        List<Comentario>comentarios = comentariosRepository.findAllBy(idIncidencia);
        return comentarios;
    }
    @RequestMapping("/usuario")//todo getUsuario
    public Usuario getUsuario(int id ){
        Usuario usuario = null;
        usuario =usuarioRepository.findOne(id);
        return usuario;
    }

    @RequestMapping("/incidecias_id")//todo incidencia por id
    public Incidencia getIncidencia(@RequestParam (value = "id",defaultValue = "0")int  id) {
        Incidencia incidencia = null;
        incidencia = incidenciaRepository.findOne(id);
        return incidencia;
    }
    @RequestMapping("/incidecias_id_u")//todo incidenciaU por id
    public IncidenciasUsuarios getIncidenciasU(int  id) {
        IncidenciasUsuarios incidenciasUsuarios = null;
        incidenciasUsuarios = incidenciasUsuariosRepository.findOne(id);
        return incidenciasUsuarios;
    }
    @RequestMapping("/borrar_una_incidencia")//todo borrar una incidencia o inciUsu
    public void eliminarUnaIncidencia(@RequestParam (value = "id",defaultValue = "0")int id){
        Incidencia incidencia = null;
        IncidenciasUsuarios incidenciasUsuarios = null;
        IncidenciaComentada incidenciaComentada = null;
        incidencia = getIncidencia(id);
        incidenciasUsuarios = getIncidenciasU(id);
        incidenciaComentada = getIncidenciaComentada(id);

        if(incidencia!=null){
            incidenciaRepository.delete(incidencia);
            eliminarComentarios(incidencia.getId());
        }
        if(incidenciasUsuarios != null){
            incidenciasUsuariosRepository.delete(incidenciasUsuarios);
            eliminarComentarios(incidenciasUsuarios.getId());

        }
        if(incidenciaComentada != null){
            incidenciaComentadaRepository.delete(incidenciaComentada);
            eliminarComentarios(incidenciaComentada.getId());

        }

    }
    private void eliminarComentarios(int idIncidencia){
        List<Comentario>comentarios = getComentariosIncidencia(idIncidencia);
        for (Comentario c: comentarios) {
            comentariosRepository.delete(c);
        }
    }



    @RequestMapping("/borrar")
    public void eliminarIncidencias(){
        incidenciaRepository.deleteAll();
        if (incidenciaRepository.findAll().size() > 0){
            System.out.println("------------------------->------------------>----------> delete all ni caso ");
            for (Incidencia i : incidenciaRepository.findAll()){
                incidenciaRepository.delete(i);
            }
        }
    }


    @RequestMapping("/borrar_incidencias_usuarios")
    public void eliminarIncidenciasUsuarios(){
        incidenciasUsuariosRepository.deleteAll();
    }
    /*
    * private int id;
    private String titulo;
    private String calle;
    private String motivo;
    private String tramo;
    private String observaciones;
    private Date inicio;
    private Date fin;
    private double longitud;
    private double latitud;
    private String descipcion;
    private String tipo;
    private int icono;
    *
    * */

    @RequestMapping("/add_incidencia")//todo añadir una incidencia
    public void addIncidencia(@RequestParam(value = "titulo", defaultValue = "nada") String titulo,
                           @RequestParam(value = "calle" , defaultValue = "no aplica") String calle,
                           @RequestParam(value = "motivo", defaultValue = "-1") String motivo,
                           @RequestParam(value = "tramo", defaultValue = "-1") String tramo,
                           @RequestParam(value = "observaciones", defaultValue = "-1") String observaciones,
                           @RequestParam(value = "inicio",defaultValue = "2010-12-12")String inicio,
                           @RequestParam(value = "fin",defaultValue = "2010-12-12")String fin,
                           @RequestParam(value = "longitud", defaultValue = "0")double longitud,
                           @RequestParam(value = "latitud", defaultValue = "0")double latitud,
                           @RequestParam(value = "descipcion", defaultValue = "-1") String descipcion,
                           @RequestParam(value = "tipo", defaultValue = "-1") String tipo) {
        Incidencia incidencia = new Incidencia();
        incidencia.setTitulo(titulo);
        incidencia.setCalle(calle);
        incidencia.setMotivo(motivo);
        incidencia.setTramo(tramo);
        incidencia.setObservaciones(observaciones);
        incidencia.setInicio(Util.parseFecha(inicio));
        incidencia.setFin(Util.parseFecha(fin));
        incidencia.setLongitud(longitud);
        incidencia.setLatitud(latitud);
        incidencia.setDescipcion(descipcion);
        incidencia.setTipo(tipo);
        incidenciaRepository.save(incidencia);
    }
    @RequestMapping("/add_incidencia_usuario")//todo añadir una incidenci de usuario
    public void addIncidenciaUsuario(@RequestParam(value = "titulo", defaultValue = "nada") String titulo,
                           @RequestParam(value = "calle" , defaultValue = "no aplica") String calle,
                           @RequestParam(value = "motivo", defaultValue = "-1") String motivo,
                           @RequestParam(value = "tramo", defaultValue = "-1") String tramo,
                           @RequestParam(value = "observaciones", defaultValue = "-1") String observaciones,
                           @RequestParam(value = "inicio",defaultValue = "2010-12-12")String inicio,
                           @RequestParam(value = "fin",defaultValue = "2010-12-12")String fin,
                           @RequestParam(value = "longitud", defaultValue = "0")double longitud,
                           @RequestParam(value = "latitud", defaultValue = "0")double latitud,
                           @RequestParam(value = "descipcion", defaultValue = "-1") String descipcion,
                           @RequestParam(value = "tipo", defaultValue = "-1") String tipo) {

        IncidenciasUsuarios incidenciasUsuarios = new IncidenciasUsuarios();
        incidenciasUsuarios.setTitulo(titulo);
        incidenciasUsuarios.setCalle(calle);
        incidenciasUsuarios.setMotivo(motivo);
        incidenciasUsuarios.setTramo(tramo);
        incidenciasUsuarios.setObservaciones(observaciones);
        incidenciasUsuarios.setInicio(Util.parseFecha(inicio));
        incidenciasUsuarios.setFin(Util.parseFecha(fin));
        incidenciasUsuarios.setLongitud(longitud);
        incidenciasUsuarios.setLatitud(latitud);
        incidenciasUsuarios.setDescipcion(descipcion);
        incidenciasUsuarios.setTipo(tipo);
        incidenciasUsuariosRepository.save(incidenciasUsuarios);
    }
    @RequestMapping("/modificar_incidencia")//todo modificar una incidencia
    public void modificarIncidencia(@RequestParam(value = "id", defaultValue = "1")int id,
                                    @RequestParam(value = "titulo", defaultValue = "nada") String titulo,
                                    @RequestParam(value = "calle" , defaultValue = "no aplica") String calle,
                                    @RequestParam(value = "motivo", defaultValue = "-1") String motivo,
                                    @RequestParam(value = "tramo", defaultValue = "-1") String tramo,
                                    @RequestParam(value = "observaciones", defaultValue = "-1") String observaciones,
                                    @RequestParam(value = "inicio",defaultValue = "2010-12-12")String inicio,
                                    @RequestParam(value = "fin",defaultValue = "2010-12-12")String fin,
                                    @RequestParam(value = "longitud", defaultValue = "0")double longitud,
                                    @RequestParam(value = "latitud", defaultValue = "0")double latitud,
                                    @RequestParam(value = "descipcion", defaultValue = "-1") String descipcion,
                                    @RequestParam(value = "tipo", defaultValue = "-1") String tipo){
        if(getIncidenciasU(id)!=null) {
            IncidenciasUsuarios incidenciasUsuarios = getIncidenciasU(id);
            incidenciasUsuarios.setTitulo(titulo);
            incidenciasUsuarios.setCalle(calle);
            incidenciasUsuarios.setMotivo(motivo);
            incidenciasUsuarios.setTramo(tramo);
            incidenciasUsuarios.setObservaciones(observaciones);
            incidenciasUsuarios.setInicio(Util.parseFecha(inicio));
            incidenciasUsuarios.setFin(Util.parseFecha(fin));
            incidenciasUsuarios.setLongitud(longitud);
            incidenciasUsuarios.setLatitud(latitud);
            incidenciasUsuarios.setDescipcion(descipcion);
            incidenciasUsuarios.setTipo(tipo);
            incidenciasUsuariosRepository.save(incidenciasUsuarios);
        }if(getIncidencia(id)!=null) {
            Incidencia incidencia = getIncidencia(id);
            incidencia.setTitulo(titulo);
            incidencia.setCalle(calle);
            incidencia.setMotivo(motivo);
            incidencia.setTramo(tramo);
            incidencia.setObservaciones(observaciones);
            incidencia.setInicio(Util.parseFecha(inicio));
            incidencia.setFin(Util.parseFecha(fin));
            incidencia.setLongitud(longitud);
            incidencia.setLatitud(latitud);
            incidencia.setDescipcion(descipcion);
            incidencia.setTipo(tipo);
            incidenciaRepository.save(incidencia);
        }

    }

    private IncidenciaComentada castIncidencia(Incidencia incidencia, IncidenciasUsuarios incidenciasUsuarios){
        IncidenciaComentada incidenciaComentada = new IncidenciaComentada();
        if(incidencia!=null) {
            incidenciaComentada.setCalle(incidencia.getCalle());
            incidenciaComentada.setTitulo(incidencia.getTitulo());
            incidenciaComentada.setMotivo(incidencia.getMotivo());
            incidenciaComentada.setTramo(incidencia.getTramo());
            incidenciaComentada.setObservaciones(incidencia.getObservaciones());
            incidenciaComentada.setInicio(incidencia.getInicio());
            incidenciaComentada.setFin(incidencia.getFin());
            incidenciaComentada.setLongitud(incidencia.getLongitud());
            incidenciaComentada.setLatitud(incidencia.getLatitud());
            incidenciaComentada.setDescipcion(incidencia.getDescipcion());
            incidenciaComentada.setTipo(incidencia.getTipo());
        }else if(incidenciasUsuarios!= null){
            incidenciaComentada.setTitulo(incidenciasUsuarios.getTitulo());
            incidenciaComentada.setCalle(incidenciasUsuarios.getCalle());
            incidenciaComentada.setMotivo(incidenciasUsuarios.getMotivo());
            incidenciaComentada.setTramo(incidenciasUsuarios.getTramo());
            incidenciaComentada.setObservaciones(incidenciasUsuarios.getObservaciones());
            incidenciaComentada.setInicio(incidenciasUsuarios.getInicio());
            incidenciaComentada.setFin(incidenciasUsuarios.getFin());
            incidenciaComentada.setLongitud(incidenciasUsuarios.getLongitud());
            incidenciaComentada.setLatitud(incidenciasUsuarios.getLatitud());
            incidenciaComentada.setDescipcion(incidenciasUsuarios.getDescipcion());
            incidenciaComentada.setTipo(incidenciasUsuarios.getTipo());
        }
        incidenciaComentada.setTieneComentarios(1);

        return incidenciaComentada;
    }
}
