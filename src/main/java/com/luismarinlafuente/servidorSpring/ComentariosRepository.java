package com.luismarinlafuente.servidorSpring;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ComentariosRepository extends CrudRepository<Comentario, Integer> {
    List<Comentario> findAll();
    List<Comentario> findAllBy(int idIncidencia);

    @Override
    Comentario findOne(Integer id);
}
