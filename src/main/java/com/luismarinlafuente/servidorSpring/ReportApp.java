package com.luismarinlafuente.servidorSpring;

import javax.persistence.*;

@Entity
@Table(name = "report_app")
public class ReportApp {

    @Id
    @GeneratedValue
    private int id;
    @Column(name = "id_usuario")
    private int idUsuario;
    @Column
    private String email;
    @Column
    private String mensaje;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
