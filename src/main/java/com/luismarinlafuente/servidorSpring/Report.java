package com.luismarinlafuente.servidorSpring;

import javax.persistence.*;

@Entity
@Table(name = "reportdas")
public class Report  {

    @Id
    @GeneratedValue
    private int id;
    @Column(name = "id_usuario")
    private int idUsuario;
    @Column(name = "id_comentario")
    private int idComentario;
    @Column(name = "motivo")
    private String motivo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdComentario() {
        return idComentario;
    }

    public void setIdComentario(int idComentario) {
        this.idComentario = idComentario;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }
}
