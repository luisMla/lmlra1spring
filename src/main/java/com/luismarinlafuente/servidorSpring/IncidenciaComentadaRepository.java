package com.luismarinlafuente.servidorSpring;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IncidenciaComentadaRepository extends CrudRepository<IncidenciaComentada, Integer> {
    List <IncidenciaComentada> findAll();
    IncidenciaComentada findAlldBy(int id);
    IncidenciaComentada findFirstBy(int id);

    @Override
    IncidenciaComentada findOne(Integer id);

    void deleteAll();

}
