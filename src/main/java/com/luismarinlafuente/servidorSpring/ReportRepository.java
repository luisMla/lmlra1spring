package com.luismarinlafuente.servidorSpring;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ReportRepository extends CrudRepository<Report, Integer> {
    List<Report> findAll();
}
