package com.luismarinlafuente.servidorSpring;

import org.springframework.data.repository.CrudRepository;

public interface ReportAppRepository extends CrudRepository<ReportApp, Integer>{

}
