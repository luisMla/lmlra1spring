package com.luismarinlafuente.servidorSpring;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {
    Usuario findAllBy(int id);
    Usuario findFirstBy(int id);
    List<Usuario> findAll();
    Usuario findUsuarioBy(String loggin, String pass);

    @Override
    Usuario findOne(Integer id);
}
